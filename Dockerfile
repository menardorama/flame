ARG TAG=2023-05-25
FROM ghcr.io/fdarveau/flame:${TAG}

RUN chown -R 1000:1000 /app

CMD ["sh", "-c", "node server.js"]